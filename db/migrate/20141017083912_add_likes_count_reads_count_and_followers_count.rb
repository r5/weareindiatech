class AddLikesCountReadsCountAndFollowersCount < ActiveRecord::Migration
  def change
    add_column :posts, :likes_count, :integer, :null => false, :default => 0
    add_column :posts, :read_count, :integer, :null => false, :default => 0
    add_column :topics, :fans_count, :integer, :null => false, :default => 0
  end
end
