class AddFullTextSearch < ActiveRecord::Migration




  def up
    add_column :posts, :search_term, :tsvector
    # TODO: Always ensure that following trigger works fine. Specially the join

    execute <<-SQL
      UPDATE posts SET search_term = (
          setweight(to_tsvector('english', coalesce(posts.title, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(posts.subtitle, '')), 'B') ||
          setweight(to_tsvector('english', coalesce(posts.content, '')), 'C')
        );
    SQL

    ActiveRecord::Migration.execute <<-SQL
      CREATE FUNCTION post_search_trigger() RETURNS TRIGGER AS $$
      BEGIN
          new.search_term = (
          setweight(to_tsvector('english', coalesce(new.title, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(new.subtitle, '')), 'B') ||
          setweight(to_tsvector('english', coalesce(new.content, '')), 'C')
        );
        return new;        
      END
      $$ LANGUAGE 'plpgsql';

      CREATE INDEX index_posts_on_search_term on posts USING gin(search_term) WHERE published = 't';

      CREATE TRIGGER update_search_term BEFORE INSERT OR UPDATE
      ON posts FOR EACH ROW EXECUTE PROCEDURE post_search_trigger();
    SQL

  end

  def down
    execute <<-SQL
      DROP INDEX index_posts_on_search_term;
    SQL
    remove_column :posts, :search_term
    execute <<-SQL
      DROP TRIGGER update_search_term on posts;
      DROP FUNCTION post_search_trigger();
    SQL
  end
end
