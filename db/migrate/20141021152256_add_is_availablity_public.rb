class AddIsAvailablityPublic < ActiveRecord::Migration
  def change
    add_column :users, :is_availablity_public, :boolean, default:  false
  end
end
