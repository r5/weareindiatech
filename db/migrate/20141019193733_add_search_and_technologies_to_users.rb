class AddSearchAndTechnologiesToUsers < ActiveRecord::Migration
  def up
    add_column :users, :technologies, :text, array: true, default: []
    add_column :users, :search_term, :tsvector
    execute <<-SQL
      UPDATE users SET search_term = (
          setweight(to_tsvector('english', coalesce(users.name, '')), 'A') || 
          setweight(to_tsvector('english', coalesce(array_to_string(users.technologies, ' '), '')), 'B') || 
          setweight(to_tsvector('english', coalesce(users.company_name, '')), 'B') || 
          setweight(to_tsvector('english', coalesce(users.one_liner, '')), 'C') || 
          setweight(to_tsvector('english', coalesce(users.website, '')), 'B') || 
          setweight(to_tsvector('english', coalesce(users.bio, '')), 'D')
        );
    SQL

    ActiveRecord::Migration.execute <<-SQL
      CREATE FUNCTION user_search_trigger() RETURNS TRIGGER AS $$
      BEGIN
          new.search_term = (
          setweight(to_tsvector('english', coalesce(new.name, '')), 'A') || 
          setweight(to_tsvector('english', coalesce(array_to_string(new.technologies, ' '), '')), 'B') ||  
          setweight(to_tsvector('english', coalesce(new.company_name, '')), 'B') || 
          setweight(to_tsvector('english', coalesce(new.one_liner, '')), 'C') || 
          setweight(to_tsvector('english', coalesce(new.website, '')), 'B') || 
          setweight(to_tsvector('english', coalesce(new.bio, '')), 'D')
        );
        return new;        
      END
      $$ LANGUAGE 'plpgsql';

      CREATE INDEX index_users_on_search_term on users USING gin(search_term);

      CREATE TRIGGER update_search_term BEFORE INSERT OR UPDATE
      ON users FOR EACH ROW EXECUTE PROCEDURE user_search_trigger();
    SQL

  end

  def down
    remove_column :users, :technologies
    execute <<-SQL
      DROP INDEX index_users_on_search_term;
    SQL
    remove_column :users, :search_term
    execute <<-SQL
      DROP TRIGGER update_search_term on users;
      DROP FUNCTION user_search_trigger();
    SQL
  end
end
