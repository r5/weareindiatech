class CreateHiringEmails < ActiveRecord::Migration
  def change
    create_table :hiring_emails do |t|
      t.integer :sender_id
      t.integer :receiver_id
      t.text :content
      t.string :email

      t.timestamps null: false
    end
    add_index :hiring_emails, :sender_id
    add_index :hiring_emails, :receiver_id
  end
end
