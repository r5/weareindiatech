class Subscription < ActiveRecord::Migration
   def self.up
    create_table :subscriptions do |t|
      t.integer :subscriber_id
      t.integer :subscribed_to_id
    end

    add_index(:subscriptions, :subscriber_id)
    add_index(:subscriptions, :subscribed_to_id)
  end

  def self.down
      remove_index(:subscriptions, :subscriber_id)
      remove_index(:subscriptions, :subscribed_to_id)
      drop_table :subscriptions
  end
end
