class AddSlugToUsersAndPostsAndTopics < ActiveRecord::Migration
  def change
    add_column :posts, :slug, :string, :unique => true
    add_column :topics, :slug, :string, :unique => true
  end
end
