class ChangeShotProfileFormatInUsers < ActiveRecord::Migration
  def up
    remove_column :users, :short_profile
    add_column :users, :short_profile, :boolean, null: false, default: true
  end

  def down
    remove_column :users, :short_profile
    add_column :users, :short_profile, :string
  end
end
