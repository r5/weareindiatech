class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :url
      t.integer :user_id
      t.text :descrption
      t.string :image
      t.string :role
      t.string :slug, null: false
      t.text :short_description

      t.timestamps null: false
    end
    add_index :projects, :user_id
  end
end
