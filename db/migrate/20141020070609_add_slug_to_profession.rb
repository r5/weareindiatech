class AddSlugToProfession < ActiveRecord::Migration
  def change
    add_column :professions, :slug, :string
  end
end
