class IsAvailableToUserAndHstore < ActiveRecord::Migration
  def up
    add_column :users, :is_available, :boolean, default: false
    add_column :users, :extras, :hstore
    add_column :users, :linked_in, :string
    add_column :users, :twitter, :string
    add_column :users, :github, :string
    add_column :users, :website, :string
    add_column :users, :company_name, :string
    add_column :users, :short_profile, :string
    add_column :users, :one_liner, :string
    # execute 'CREATE INDEX index_users_extras ON users USING gin(extras)'
  end

  def down
    remove_column :users, :is_available
    remove_column :users, :extras
    remove_column :users, :linked_in
    remove_column :users, :twitter
    remove_column :users, :github
    remove_column :users, :website
    remove_column :users, :company_name
    remove_column :users, :short_profile
    remove_column :users, :one_liner
    # execute 'DROP INDEX index_users_extras'
  end
end
