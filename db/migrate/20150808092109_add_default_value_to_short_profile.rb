class AddDefaultValueToShortProfile < ActiveRecord::Migration
  def up
    change_column :users, :short_profile, :boolean, :default => false
  end

  def down
    change_column :users, :short_profile, :boolean, :default => true
  end
end
