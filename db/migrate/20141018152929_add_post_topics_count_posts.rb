class AddPostTopicsCountPosts < ActiveRecord::Migration
  def up
    add_column :topics, :post_topics_count, :integer, null: false, default: true
  end
end
