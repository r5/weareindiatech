class AddProfessionNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :profession_name, :string
  end
end
