# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[["Startups", "Whether you think you can, or think you can’t — you’re right eitther way"],
["Funding", "An entrepreneur without funding is a musician without an instrument"],
["TechCheck", "Latest technology trends, smartphones and much more"],
["Ecommerce", "There is a long way to go from here"],
["Entrepreneurship", "I'm an entrepreneur. 'Ambitious' is my middle name"],
["Ruby on Rails", "What sets this framework apart from all of the others is the preference for
convention over configuration making applications easier
to develop and understand."],
["JavaScript", "JavaScript will stay relevant as long as people use the internet."]]





User.create({"id"=>1, "email"=>"vipin.itm@gmail.com", "password"=>"password", "password_confirmation" => 'password', "name"=>"Vipin Nagpal", "username"=>"nagpal", "profession_id"=>1, "is_available"=>true, "linked_in"=>"", "twitter"=>"", "github"=>"", "website"=>"vipinnagpal.com", "company_name"=>"Sqooz", "short_profile"=>"As they say, every person has a meadow of flairs; I think mine lies in coding. I am a Full Stack Ruby on Rails developer with almost 4 years or experience. During my career I have worked on projects for Disney, Chipotle, MyTime.com(Melian Labs), WeWork and more.", "one_liner"=>"Greatness comes from trust", "technologies"=>["Ruby on Rails", "Ruby", "Javascripts"], "is_availablity_public"=>true})