module ApplicationHelper


  def follow_or_unfollow(resource, extra_classes="", following_ids=[], count=false)
    unless current_user
      link_to "<span class='text'>Follow</span>".html_safe, user_session_path(alert: 'follow'), class: "btn btn-primary login #{extra_classes}"
    else
      if following_ids.include?(resource.id)
        unfollow_path = topic_path(resource, unfollow: true)
        link_to "<span class='text'>Unfollow</span>".html_safe, unfollow_path, method: :put, class: "btn btn-sm btn-danger unfollow-btn #{extra_classes}", remote: true, data: {type: 'json'}
      else
        follow_path = topic_path(resource, follow: true)
        link_to "<span class='text'>Follow</span>".html_safe, follow_path, method: :put, class: "btn btn-sm  btn-primary follow-btn #{extra_classes}", remote: true, data: {type: 'json'}
      end
    end
  end

  def subscribe_or_not(user)
    unless current_user
      link_to "Get email when there is new post from #{user.name}", user_session_path(alert: 'sub'), class: "btn btn-primary login"
    else
      unless current_user.subscribed_already?(user)
        link_to "Get email when there is new post from #{user.name}", subscribe_user_path(user), class: "btn btn-primary",  data: {type: 'json', disabled_with: "Get email when there is new post from #{user.name}"}, remote: true, id: 'subscribe-link', method: :post
      else
        link_to 'Unsubscribe', unsubscribe_user_path(user), class: 'btn btn-warning',  data: {type: 'json', disabled_with: "Unsubscribe"}, remote: true, id: 'unsubscribe-link', method: :post
      end
    end
  end

  def tweet_referral_link_url(post)
    url = "&url=" + post.short_url
    text = "&text=#{post.title}"
    "https://twitter.com/intent/tweet?"  + url + text + "&related=weareindiatech"
  end

  def facebook_referral_link_url(post)
    url = "&p[url]=" + seo_post_url(post.user, post)
    title = "&p[title]=WeAreIndiaTech"
    image = "&p[images][0]=#{post.primary_asset_url}"
    summary = "&p[summary]=#{post.title}"
    "https://www.facebook.com/share.php?s=100" + url + image + title + summary
  end

  
end
