module SeoHelper

  def canonical(link)
    set_meta_tags canonical: link
  end

  def title(title)
    set_meta_tags :site => 'India Tech', title: title, :reverse => true
  end

  def description(description)
    set_meta_tags description: description
  end

  def keywords(keywords)
    set_meta_tags keywords: keywords
  end
end