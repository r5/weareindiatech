$(document).ready(function () {
  $('#project_description').redactor({
    minHeight: 350,
    placeholder: 'What this project is about',
    formatting: ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
  });
});