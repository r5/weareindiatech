function updateFollowUnfollow($that,data){
  $that.find('span.count').text(data.count);
  if(data.follow){
    $that.removeClass('follow-btn btn-primary');
    $that.addClass('unfollow-btn btn-danger');
    $that.find('span.text').text('Unfollow');
    $that.attr('href', $that.attr('href').replace(/\?follow/, '?unfollow'));
  }else if(data.unfollow){
    $that.addClass('follow-btn btn-primary');
    $that.removeClass('unfollow-btn btn-danger');
    $that.find('span.text').text('Follow');
    $that.attr('href', $that.attr('href').replace(/\?unfollow/, '?follow'));
  }
} 

function showAlert(msg, cls){
  $(".container#flash-bar").show().html("").append('<div class="alert alert-'+ cls +'"><a data-dismiss="alert" class="close">&#215</a><div id = "flash_'+ cls +'">'+ msg +'</div></div>')
}

$(document).on('ajax:success', '.follow-btn, .unfollow-btn', function(e, data){
  updateFollowUnfollow($(this), data)
});

$(document).on('ajax:error', '.follow-btn, .unfollow-btn', function(e, data){
  showAlert("There was an error. Please try again.")
});


// $(document).on('click', '.login', function(){
//   $('#signin-modal').modal('toggle');
//   return false;
// });


$(document).ready(function () {

  setTimeout(function(){ $('#flash-bar').fadeOut(1000)}, 6000)

  $('.post-content img').addClass('img-responsive')

  $('#search-form').on('submit' ,function(){
    Turbolinks.visit($(this).attr('action') + '?q=' + $(this).find("input#q").val())
    return false;
  });
});