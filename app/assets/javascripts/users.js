$(document).ready(function () {
  $('.select-writer-form, .select-reader-form').on('click', function () {

    $('.writer-form').toggleClass("hide");
    $('.reader-form').toggleClass("hide");

    $('.select-reader-form').toggleClass("active");
    $('.select-writer-form').toggleClass("active");
  });

  $('textarea#user_bio').redactor({
    minHeight: 150,
    formatting: ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
    buttons: ['html', 'formatting', 'bold', 'italic', 'deleted',
'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment', 'horizontalrule']
  });

  $("#user_profession_id").on('change', function(){
    if($(this).find('option:selected').text() == 'Other'){
      $('#user_profession_name').parent().removeClass("hide");
    }else{
      $('#user_profession_name').parent().addClass("hide");
    }
  });

  $("#edit_user, #new_user").on("submit", function(event){
    var tech_f = $(this).find("#user_technologies");
    if(tech_f.val() && tech_f.val().length > 0){
      var techs = tech_f.val().split(", ");
      var str = ""
      jQuery.each(techs, function(index, val){
        str = str + "<input type='hidden' name='user[technologies][]' value='"+ val +"'>"
      });
      $(this).prepend(str)
    }
    tech_f.remove();
  });
});
