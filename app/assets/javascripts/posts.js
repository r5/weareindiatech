$(document).ready(function () {

  $('#js-save-draft').on('click', function(){
    $('input#post_published').val('false');
    $('form.new_post, form.edit_post').submit();
    return false;
  });

  $(".publish-post-link").on('ajax:success', function(){
    $(this).replaceWith("<span class='label label-success'>Published <i class='entypo-publish'></i></span>")
  })

  $('#like-post').on('ajax:success', function(){
    $(this).remove()
  });

  $('#unsubscribe-link').on('ajax:success', function(){
    $(this).remove()
  });

  $('#subscribe-link').on('ajax:success', function(){
    $(this).remove()
  });



  $('#post_content').redactor({
    minHeight: 350,
    placeholder: 'Your post here',
    imageUpload: '/post_assets',
    formatting: ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
    // autosave: '/posts/autosave',
    // autosaveName: 'post',
    // autosaveInterval: 40,
    // autosaveCallback: function(name, json){
      
    // },
    imageUploadCallback: function(image, json){
      if(json.ok){
        image.attr('id', json.post_asset_id)
        $('form#new_post, form#edit_form').prepend('<input type="hidden" name="post[post_asset_ids][]" value="'+ json.post_asset_id +'" />')
      }
    },
    imageDeleteCallback: function(url, image){
      $.ajax({
        url: '/post_assets/' + image.attr('id'),
        type: 'delete'
      });
    }
  });
}); 