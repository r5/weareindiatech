# encoding: utf-8

class ProjectUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  def default_url
    ActionController::Base.helpers.asset_path("fallback/#{version_name}_missing.jpg")
  end

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  version :medium do
    process :resize_to_fill => [200,250]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end


end
