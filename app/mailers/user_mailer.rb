class UserMailer < ActionMailer::Base
  default from: "India Tech Support <sayhi@weareindiatech.com>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.welcome.subject
  #
  def welcome(user)
    @user = user
    mail to: @user.email, subject: 'Welcome to the Community [WeAreIndiaTech]', from: 'Vipin Nagpal <sayhi@weareindiatech.com>'
  end

  def subscription_email(subscriber, post, subscribed_to)
    @subscriber = subscriber
    @post = post
    @subscribed_to = subscribed_to
    mail to: @subscriber.email, subject: "New post from #{subscribed_to.name} [WeAreIndiaTech]"
  end

  def hiring_email(hiring_email)
    @sender = hiring_email.sender
    @receiver = hiring_email.receiver
    @hiring_email = hiring_email
    mail to: @receiver.email, subject: "#{@sender.name} Showed Intrest in Yout India Tech Profile", from: @hiring_email.email
  end

end
