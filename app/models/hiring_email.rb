class HiringEmail < ActiveRecord::Base


  validates_presence_of :sender_id, :email, :receiver_id
  validate :max_two_emails
  validate :hring_status_public
  after_create :send_message

  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'


private
  
  def max_two_emails
    self.errors[:base] << "You have already sent two emails to this member, please wait for the response. For help please contact admin." and return false if HiringEmail.where(sender_id: sender_id, receiver_id: receiver_id).count == 2
  end

  def hring_status_public
    self.errors[:base] << "Sorry, we can not send message to #{receiver.name}." and return false unless self.receiver.is_availablity_public
  end

  def send_message
    UserMailer.hiring_email(self).deliver_later
  end
end
