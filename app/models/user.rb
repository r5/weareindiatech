class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  # store_accessor :extras, :technologies, :other_professions

  after_commit :welcome_message, on: :create

  attr_accessor :login
  has_many :posts
  has_many :post_assets
  has_many :likes

  has_many :subscriptions, :foreign_key => 'subscribed_to_id'
  has_many :subscribers, through: :subscriptions

  has_many :user_subscribed, :foreign_key => 'subscriber_id', class_name: 'Subscription'
  has_many :subscribed_tos, through: :user_subscribed, :source => :subscribed_to

  acts_as_follower

  has_many :hiring_emails, :foreign_key => 'sender_id'

  belongs_to :profession
  # before_save :fix_short_profile

  has_many :projects, dependent: :destroy
  validate :profession_present

  extend FriendlyId
  friendly_id :username, use: :slugged

  mount_uploader :avatar, AvatarUploader

  validates :username, presence: true, length: {maximum: 255}, format: { with: /\A[a-zA-Z0-9]*\z/, message: "may only contain letters and numbers." }
  validates_uniqueness_of :username, case_sensitive: false, message: 'allready taken'
  validates :one_liner, presence: true

  accepts_nested_attributes_for :projects, allow_destroy: true
  include PgSearch
  pg_search_scope :search,
                  :against => {
                    :name => 'A',
                    :technologies => 'B',
                    :company_name => 'B',
                    :one_liner => 'C',
                    :website => 'B',
                    :bio => 'D'
                    },
                  :using => {
                    :tsearch => {:prefix => true, :dictionary => "english", :tsvector_column => 'search_term'}
                  }

  include Countable
  
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def seo_title
    "#{self.name} profile"
  end

  def seo_description
    self.one_liner.present? ? self.one_liner : "Profile created by #{self.name} on"
  end

  def seo_keywords
    []
  end

  def subscribed_already?(user)
    Subscription.where(subscriber_id: self.id, subscribed_to_id: user.id).pluck(:id)[0]
  end

  def subscribe_to(user)
    Subscription.create(subscriber_id: self.id, subscribed_to_id: user.id) unless self.subscribed_already?(user) 
  end

  def unsubscribe_from(user)
    Subscription.where(subscriber_id: self.id, subscribed_to_id: user.id).delete_all
  end

  def like?(post)
    Like.where(post_id: post.id, user_id: self.id).pluck(&:id)[0]
  end

  def formatted_profession
    (profession && profession.name == 'Other') ? profession_name : profession.name
  end

  def like(post)
    Like.create(post_id: post.id, user_id: self.id)
    post.increment_count(:likes_count)
  end
private

  def welcome_message
    UserMailer.welcome(self).deliver_later
  end

  # def fix_short_profile
  #   if self.name.present? && self.bio.present?
  #     self.short_profile = false
  #   else
  #     self.short_profile = true
  #   end
  #   true
  # end

  def profession_present
    self.errors[:base] << "Designation name can't be blank" if self.profession && self.profession.try(:name) == 'Other' && !self.profession_name.present?
  end

end
