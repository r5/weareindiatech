class PostAsset < ActiveRecord::Base

  mount_uploader :passet, PassetUploader
  
  belongs_to :user
  belongs_to :post
end
