class Post < ActiveRecord::Base

  belongs_to :user

  include PgSearch
  has_many :post_assets

  has_many :post_topics, dependent: :destroy
  has_many :topics, through: :post_topics

  default_scope {select([:id, :content, :title, :user_id, :created_at, :updated_at, :subtitle, :published, :slug, :duration,:likes_count, :read_count, :tags, :published_at])}
  extend FriendlyId
  friendly_id :title, use: :slugged
  has_many :likes

  validates :topic_ids, :title, :content, :user_id, presence: true
  before_save :set_duration
  after_save :set_published_at
  scope :published, -> { where(published: true) }
  scope :not_published, -> { where(published: false) }
  include Countable

    pg_search_scope :post_search,
                  :against => {
                    :title => 'A', 
                    :subtitle => 'B', 
                    :content => 'C'
                    },
                  :using => {
                    :tsearch => {:prefix => true, :dictionary => "english", :tsvector_column => 'search_term'}
                  }

    def short_url
      "#{ENV['WEBSITE']}/#{self.id}"
    end

    def primary_asset_url
      ps = self.post_assets.first
      ENV['WEBSITE'] + ps.passet.url if ps
    end

    def seo_title
      self.title.truncate(35)
    end

    def seo_tags
      self.tags + self.topics.map(&:title)
    end

    def seo_description
      self.subtitle.present? ? self.subtitle.truncate(100) : Rails::Html::FullSanitizer.new.sanitize(self.content).truncate(200)
    end
private
  def set_duration
    if self.new_record? || self.content_changed?
      words_count = Rails::Html::FullSanitizer.new.sanitize(self.content).scan(/\w+/).size
      self.duration = (words_count/200.0).ceil
    end
  end

  def set_published_at
    if self.published && !self.published_at
      self.touch(:published_at)
      user = self.user
      post = self
      user.subscribers.each do |sub|
        UserMailer.subscription_email(sub, post, user).deliver_later
      end
    end
  end
  
end
