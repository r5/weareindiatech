class Topic < ActiveRecord::Base

  has_many :post_topics, dependent: :destroy
  has_many :posts, through: :post_topics

  mount_uploader :splash, SplashUploader

  extend FriendlyId
  friendly_id :title, use: :slugged

  acts_as_followable

  include Countable
  
end
