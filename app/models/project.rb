class Project < ActiveRecord::Base

  belongs_to :user

  validates :name, :image, :descrption, :role, presence: true
  mount_uploader :image, ProjectUploader

  validates :url, :presence => true, :url => true

  extend FriendlyId
  friendly_id :name, use: :slugged

  before_save :set_short_description


  def description
    self.descrption
  end

private

  def set_short_description
    if self.new_record? || self.descrption_changed?
      self.short_description = Rails::Html::FullSanitizer.new.sanitize(self.descrption).truncate(170)
    end
  end

end
