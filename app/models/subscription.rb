class Subscription < ActiveRecord::Base
  belongs_to :subscriber, :class_name => 'User'
  belongs_to :subscribed_to, :class_name => 'User' , inverse_of: :user_subscribed
end