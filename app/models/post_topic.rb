class PostTopic < ActiveRecord::Base

  belongs_to :post
  belongs_to :topic, counter_cache: true
end
