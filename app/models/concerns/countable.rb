module Countable
  extend ActiveSupport::Concern

  included do
  end

  def increment_count(column_name)
    self.class.increment_counter(column_name, self.id)
  end

  def decrement_counts(column_name)
    self.class.decrement_counter(column_name, self.id)
  end
end