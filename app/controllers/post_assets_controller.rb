class PostAssetsController < ApplicationController

  skip_before_filter :verify_authenticity_token, :only => :create
  before_action :authenticate_user!
  
  def create
    pa = PostAsset.new(passet: params[:file], user_id: current_user.id)
    if pa.save
      render json: {filelink: pa.passet.url, post_asset_id: pa.id, ok: true}
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def destroy
    pa = current_user.post_assets.find_by(id: params[:id])
    pa.destroy
    render json: {}
  end

private



end
