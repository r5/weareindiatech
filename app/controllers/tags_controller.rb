class TagsController < ApplicationController
  def show
    @tag = params[:id]
    @posts = Post.where("? = ANY (tags)", params[:id]).published.page(params[:page]).order('created_at desc').includes(:topics, :user)
  end
end
