class UsersController < ApplicationController

  before_action :find_user, only: [:show, :posts, :drafts, :likes, :subscribe, :unsubscribe, :hireme]
  before_action :authenticate_user!, only: [:drafts, :subscribe, :unsubscribe, :hireme]

  def index
    unless params[:q]
      @users = User.page(params[:page]).includes(:profession).order("created_at desc")
    else
      @users = User.search(params[:q]).page(params[:page]).includes(:profession)
    end
  end

  def pindex
    @profession = Profession.friendly.find(params[:profession_id])
    unless params[:q]
      @users = User.where(profession_id: @profession.id).page(params[:page]).includes(:profession).order("created_at desc")
    else
      @users = User.where(profession_id: @profession.id).search(params[:q]).page(params[:page]).includes(:profession)
    end
  end

  def hireme
    if request.post?
      @hiring_email = HiringEmail.new(hiring_email_params)
      @hiring_email.sender_id = current_user.id
      @hiring_email.receiver_id = @user.id
      if @hiring_email.save
        redirect_to user_path(@user), notice: "Message successfully sent to #{@user.name}"
      else
        render 'hireme'
      end
    else
      @hiring_email = HiringEmail.new(email: current_user.email)
      render 'hireme'
    end
  end

  def show
    @top_posts = @user.posts.published.limit(5).order('likes_count desc')
    @projects = @user.projects
  end

  def dashboard
  end

  def likes
    @likes = @user.likes.page(params[:page]).order("created_at desc").includes(:post)
  end

  def subscribe
    current_user.subscribe_to(@user)
    render json: {}
  end

  def unsubscribe
    current_user.unsubscribe_from(@user)
    render json: {}
  end

  def posts
    @posts = @user.posts.published.page(params[:page]).order("created_at desc")
  end

  def drafts
    @posts = @user.posts.not_published.page(params[:page]).order("created_at desc")
  end

private 

  def hiring_email_params
    params.require(:hiring_email).permit(:content, :email)
  end
  
  def find_user
    @user = User.friendly.find(params[:id])
    not_found unless @user
  end
end
