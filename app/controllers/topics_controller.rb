class TopicsController < ApplicationController

  layout 'sidebar'

  before_action :find_topic, except: [:new, :create]
  before_action :only_vipin, only: [:new, :create, :edit]

  def show
    @following_ids = current_user ? current_user.follows.pluck("followable_id") : []
    @posts = @topic.posts.page(params[:page]).order('created_at desc').includes(:topics, :user)
  end

  def new
    @topic = Topic.new
    render :new, layout: 'application'
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save
      redirect_to topic_path(@topic)
    else
      render :new, layout: 'application'
    end
  end

  def edit
    render :edit, layout: 'application'
  end

  def update
    if params.has_key?(:follow)
      @follow = true
      current_user.follow(@topic)
      @topic.increment_count('fans_count')
    elsif params.has_key?(:unfollow)
      @unfollow = true
      current_user.stop_following(@topic)
      @topic.decrement_counts('fans_count')
    else
      @topic.update_attributes(topic_params) if current_user.email == 'vipin.itm@gmail.com'
    end
    respond_to do |format|
      format.js{ render json: {follow: @follow, unfollow: @unfollow, count: @topic.fans_count} ,status: :created }
      format.html{ redirect_to topic_path(@topic) }
    end
  end

private

  def find_topic
    @topic = Topic.friendly.find(params[:id])
    not_found unless @topic
  end

  def topic_params
    params.require(:topic).permit(:title, :description, :splash, :id)
  end

  def only_vipin
    unless current_user.try(:email) == 'vipin.itm@gmail.com'
      not_found
    end
  end

end
