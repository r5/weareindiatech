class HomeController < ApplicationController

  layout 'sidebar'
  
  def index
    @posts = Post.published.limit(5).order('likes_count desc')
    render :index, layout: 'application'
  end

  def about
  end

  def privacy
    render 'privacy', layout: 'application'
  end

  def tos
    render 'tos', layout: 'application'
  end
end
