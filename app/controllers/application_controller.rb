class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :alert_redirect

  after_filter :store_location
  

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def alert_redirect
    if !user_signed_in? && params[:alert].present? && t("alerts.#{params[:alert]}", default: '').present?
      redirect_to request.path, notice: t("alerts.#{params[:alert]}")
    end
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    if (request.fullpath != "/users/sign_in" &&
        request.fullpath != "/users/sign_up" &&
        request.fullpath != "/users/password" &&
        request.fullpath != "/users/sign_out" &&
        !request.xhr?) # don't store ajax calls
      session["user_return_to"] = request.fullpath if request.get?
    end
  end

  def after_sign_in_path_for(resource)
    sign_in_url = url_for(:action => 'new', :controller => 'sessions', :only_path => false, :protocol => 'http')
    sign_up_url = url_for(:action => 'new', :controller => 'registrations', :only_path => false, :protocol => 'http')
    if request.referer == sign_in_url || request.referer == sign_up_url
      super
    else
      stored_location_for(resource) || request.referer || root_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username,:name, :email, :password, :password_confirmation, :remember_me, :bio, :avatar, :profession_id,  :technologies, :other_professions, :is_available, :github, :linked_in, :twitter, :website, :short_profile, :company_name, :one_liner, :name, :avatar_cache, :profession_name, :is_availablity_public, technologies: []) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username,:name, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password, :bio, :avatar, :profession_id, :technologies, :other_professions, :is_available, :github, :linked_in, :twitter, :website, :company_name, :one_liner, :name, :avatar_cache, :profession_name, :is_availablity_public, technologies: []) }
  end
end
