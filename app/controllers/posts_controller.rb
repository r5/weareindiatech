class PostsController < ApplicationController
  before_action :set_post, only: [:show, :like, :increment_read_count]
  before_action :set_user_post, only: [:edit, :update, :destroy, :publish]
  before_action :authenticate_user!, only: [:create, :edit, :new, :update, :destroy, :autosave, :feed, :publish, :like]
  before_action :full_profile_user, only: [:new]
  before_action :get_topics, only: [:new, :edit, :topics]
  before_action :parse_tags, only: [:create, :update]

  def index
    @posts = Post.published.page(params[:page]).order('created_at desc').includes(:topics, :user)
    respond_to do |format|
      format.html {}
      format.json {render json: @posts}
    end
  end

  def increment_read_count
    @post.increment_count(:read_count)
    render json: {}
  end

  def top
    @posts = Post.published.page(params[:page]).order('likes_count').includes(:topics, :user)
    respond_to do |format|
      format.html { render 'index'}
      format.json {render json: @posts}
    end
  end

  def feed
    @posts = Post.published.distinct.joins(:post_topics).where("post_topics.topic_id in (?)", current_user.follows.pluck("followable_id") ).page(params[:page]).order("created_at desc").includes(:topics, :user)
  end

  def publish
    @post.update_attributes(published: true)
    render json: {}
  end

  def topics
    @following_ids = current_user ? current_user.follows.pluck("followable_id") : []
  end

  def explore
    if current_user
      Post.published.joins(:post_topics).where("post_topics.topic_id in (?)", current_user.follows.pluck("followable_id") )
    else
      Post.published
    end
  end

  def show
    @likes = @post.likes.includes(:user).limit(10).order("created_at desc")
    @user = @post.user
    respond_to do |format|
      format.html {}
      format.json {render json: @post}
    end
  end

  def like
    current_user.like(@post)
    render json: {}
  end

  def autosave
    
  end

  def search
    @posts = Post.published.post_search(params[:q]).includes(:topics, :user).page(params[:page])
    respond_to do |format|
      format.html { render 'explore'}
      format.json {render json: @posts}
    end
  end

  def new
    @post = Post.new
    respond_to do |format|
      format.html {}
      format.json {render json: @post}
    end
  end

  def edit
    respond_to do |format|
      format.html {}
      format.json {render json: @post}
    end
  end

  def create
    @post = current_user.posts.build(post_params)
    
    respond_to do |format|
      if @post.save
        format.html {redirect_to posts_user_path(current_user), notice: "Post sucessfully created."}
        format.json {render json: @post}
      else
        get_topics
        format.html {render 'new'}
        format.json {render json:{}, status: :unprocessable_enity}
      end
    end
  end

  def update
    @post.update(post_params)
    respond_to do |format|
      if @post.update(post_params)
        format.html {redirect_to posts_user_path(current_user), notice: "Post sucessfully updated."}
        format.json {render json: @post}
      else
        get_topics
        format.html {render 'edit'}
        format.json {render json:{}, status: :unprocessable_enity}
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html {redirect_to posts_path, notice: "Post sucessfully destroyed."}
      format.json {render json: {}}
    end
  end

  private
    def set_post
      if params[:id].match(/^[0-9]+$/)
        post = Post.find_by(id: params[:id])
        redirect_to(seo_post_path(post.user, post), :status => :moved_permanently) if post
      end

      @post = Post.published.friendly.find(params[:id])
      not_found unless @post
    end

    def set_user_post
      @post = current_user.posts.friendly.find(params[:id])
      not_found unless @post
    end

    def post_params
      params.require(:post).permit(:content, :title, :subtitle, :user_id, :published, topic_ids: [], tags: [])
    end

    def full_profile_user
      # redirect_to edit_user_registration_path, notice: 'Please tell us more about you before you add a post.' if current_user.short_profile
    end

    def get_topics
      @topics = Topic.all.order('title')
    end

    def parse_tags
      params[:post][:tags] = params[:post][:tags].split(",").map(&:strip).compact if params[:post][:tags].present?
    end
end
