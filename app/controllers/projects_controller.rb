class ProjectsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :find_project, only: [:show]
  before_action :find_user_project, only: [:edit, :update, :destroy]

  def show
    @user = @project.user
  end

  def create
    @project = current_user.projects.build(project_params)
    respond_to do |format|
      if @project.save
        format.html {redirect_to user_path(current_user), notice: "Project sucessfully created."}
        format.json {render json: @project}
      else
        format.html {render 'new'}
        format.json {render json:{}, status: :unprocessable_enity}
      end
    end
  end

  def destroy
    if @project.destroy
      redirect_to user_path(current_user), notice: "Project destroyed successfully."
    else
      redirect_to user_path(current_user), notice: "Project could not be destroyed. There was an error."
    end
  end

  def update
    respond_to do |format|
      if @project.update_attributes(project_params)
        format.html {redirect_to user_path(current_user), notice: "Project sucessfully updated."}
        format.json {render json: @project}
      else
        format.html {render 'new'}
        format.json {render json:{}, status: :unprocessable_enity}
      end
    end
  end

  def edit
  end

  def new
    @project = Project.new(user_id: current_user.id)
  end

private
  
  def find_project
    @project = Project.friendly.find(params[:id])
    not_found unless @project
  end

  def find_user_project
    @project = current_user.projects.friendly.find(params[:id])
    not_found unless @project
  end

  def project_params
    params.require(:project).permit(:user_id, :descrption, :name, :image, :url, :role)
  end
end
