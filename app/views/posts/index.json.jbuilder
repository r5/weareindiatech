json.array!(@posts) do |post|
  json.extract! post, :id, :content, :title
  json.url seo_post_url(post.user, post, format: :json)
end
