Rails.application.routes.draw do


  devise_for :users
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  get '/about' => 'home#about', as: :about
  get '/privacy' => 'home#privacy', as: :privacy
  get '/tos' => 'home#tos', as: :tos
  get '/:profession_id/u' => 'users#pindex', as: :profession_users
  get '/:user_id/projects/:id' => 'projects#show', as: :user_project

  resources :projects, only: [:create, :edit, :new, :update, :destroy]

  resources :users, only: [:index, :show] , path: 'u' do
    member do
      get 'dashboard'
      get 'likes'
      get 'posts'
      get 'hireme'
      post 'hireme'
      get 'drafts'
      post 'subscribe'
      post 'unsubscribe'
    end
  end

  resources :tags, only: [:show], path: 'tag' do
  end

  
  resources :posts, only: [:create, :edit, :update, :destroy, :new, :index] do
    collection do
      post :autosave
      get 'search'
      get 'explore'
      get 'topics'
      get 'feed'
      get 'top'
    end
    
    member do
      post 'publish'
      post 'like'
      post 'rcount' => 'posts#increment_read_count'
    end
  end
  resources :topics, only: [:show, :update, :new, :edit, :create], path: 't'
  get '/search' => 'posts#search', as: 'search'
  get ':user_id/:id' => 'posts#show', as: 'seo_post'

  resources :likes, only: [:index, :update, :destroy, :create]

  resources :post_assets, only: [:create, :destroy]

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
