# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://weareindiatech.com"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do

  add '/posts'
  add '/posts/topics'
  Profession.find_each do |pro|
    add profession_users_path(profession_id: pro.slug), :changefreq => 'weekly', :priority => 0.5
  end

  Post.includes(:user).find_each do |post|
    add seo_post_path(post.user, post), :priority => 1
  end

  User.find_each do |user|
    add user_path(user), priority: 0.75, :changefreq => 'monthly'
  end
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
