if Rails.env.production?
  PumaWorkerKiller.config do |config|
    config.ram           = 2024 # mb
    config.frequency     = 20    # seconds
    config.percent_usage = 0.97
  end

  PumaWorkerKiller.start
end